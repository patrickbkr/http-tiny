#!/usr/bin/env raku

use Test;
use HTTP::Tiny;

unless %*ENV<ONLINE_TESTING> {
    say '1..0 # SKIP: ONLINE_TESTING not set';
    exit;
}

if ( try require Cookie::Jar ) === Nil {
    say '1..0 # SKIP: Test requires Cookie::Jar module';
    exit;
}

my &from-json = { Rakudo::Internals::JSON.from-json: $^a }
my &to-json = { Rakudo::Internals::JSON.to-json: $^a }

my $cookie-jar = Cookie::Jar.new;
my $ua = HTTP::Tiny.new: :throw-exceptions, :$cookie-jar;

do-test 'Set cookie', 'GET', 'http://httpbin.org/cookies/set?foo=123&bar=456', {
    foo => '123',
    bar => '456',
}

do-test 'Delete cookie', 'GET', 'http://httpbin.org/cookies/delete?foo', {
    bar => '456',
}

done-testing;

sub do-test ( $label, $method, $url, %want ) {
    my $res = $ua.request: $method, $url;
    my $content = from-json $res<content>.decode;
    my %have = %want.keys.map: { $_ => $content<cookies>{$_} }

    subtest "$method $url: $label" => {
        ok $res<success>, 'Request was succesful';
        is-deeply %have, %want, 'Matches expected output';

    }
}
