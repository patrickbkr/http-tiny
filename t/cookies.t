#!/usr/bin/env raku

use Test;
use HTTP::Tiny;

class Good::Jar {
    method add    ( Str:D $, Str:D $ --> Bool ) { }
    method header ( Str:D $, Str:D $ --> Str  ) { }
}

ok HTTP::Tiny.new( cookie-jar => Good::Jar.new ), 'Can take cookie jar';

subtest 'Validates cookie jar' => {
    class Bad::Jar { }
    class Bad::Jar::NoAdd    { method header {} }
    class Bad::Jar::NoHeader { method    add {} }

    class Bad::Jar::BadAdd {
        method add () {}
        method header ( Str:D $, Str:D $ --> Str ) { }
    }

    class Bad::Jar::BadHeader {
        method header () {}
        method add ( Str:D $, Str:D $ --> Bool ) { }
    }

    throws-like { HTTP::Tiny.new: cookie-jar => Bad::Jar.new },
        X::AdHoc, message => /"missing the 'add' and 'header' methods"/;

    throws-like { HTTP::Tiny.new: cookie-jar => Bad::Jar::NoAdd.new },
        X::AdHoc, message => /"missing the 'add' method"/;

    throws-like { HTTP::Tiny.new: cookie-jar => Bad::Jar::BadHeader.new },
        X::AdHoc, message => /"its 'header' method does not support"/;

    throws-like { HTTP::Tiny.new: cookie-jar => Bad::Jar::BadAdd.new },
        X::AdHoc, message => /"its 'add' method does not support"/;

    throws-like { HTTP::Tiny.new: cookie-jar => Bad::Jar::NoHeader.new },
        X::AdHoc, message => *.contains: "missing the 'header' method";
}

done-testing;
